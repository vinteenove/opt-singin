<?php

use LliurePanel\ll;
use Token\Token;
use Persona\Persona;

global $personaData, $_ll;

/* @var array $configs  */
/* @var bool $resetPassword  */
/* @var string|bool $id  */
/* @var string|bool $token  */

\Bootstrap\Bootstrap::build();
\Funcoesjs\Funcoesjs::build();
\GridSystem\GridSystem::build();
\Api\Vigile\Vigile::build();
\FontAwesome\FontAwesome::build();
\Etc\Persona\PersonaTheme::bootstrap();

Persona::style(__DIR__ . '/' . basename(__FILE__, 'php') . 'css.php?' . http_build_query($personaData['color'])); ?>

<section id="singin" class="singin-layout-adminlte">
    <div class="centerVertical">
        <header><br></header>
        <div class="container">
            <div class="row">
                <div class="sm-3-7 sm-offset-2-7 md-4-10 md-offset-3-10">
                    <div class="loginBox">
                        <div class="modal modal-contact" style="display: block !important;">
                            <div class="modal-content">
                                <div class="modal-header pb-2 justify-content-center">
                                    <h1 class="text-center">
                                        <span id="lliurelogo" style="display: inline-block; width: 60%; "><?php require $personaData['logo']; ?></span>
                                    </h1>
                                </div>

                                <div id="carouselLoginSteps" class="carousel slide" data-ride="carousel" data-interval="false">
                                    <div class="carousel-inner">

                                        <div class="carousel-item<?php echo (($resetPassword)? '': ' active'); ?>">
                                            <form id="formUser" action="<?php echo ll::$data->url->endereco; ?>loginExist" method="post">
                                                <fieldset>
                                                    <div class="modal-body py-0">
                                                        <h2 class="text-center h3 pb-3">
                                                            Fazer login<br>
                                                            <small class="d-block"><small><small>Use seu usuário no sistema</small></small></small>
                                                        </h2>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <div class="input-group-text">
                                                                        <i class="fas fa-user"></i>
                                                                    </div>
                                                                </div>
                                                                <input type="text" name="login" class="form-control" id="formLoginInputUsuario" placeholder="Usuário">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="text-right">
                                                            <button type="submit" class="btn btn-primary btn-lliure text-uppercase">
                                                                <span class="btn-status-default">login</span>
                                                                <span class="btn-status-disabled">Loading...</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>

                                        <div class="carousel-item">
                                            <form id="formPassword" action="<?php echo ll::$data->url->endereco; ?>login" method="post">
                                                <fieldset>
                                                    <div class="modal-body py-0">
                                                        <h2 class="text-center h3 pb-1">Olá!</h2>
                                                        <div class="form-group text-center">
                                                            <button data-toggle="trocarUser" type="button" class="btn btn-outline-dark" style="border-radius: 100px;"><span data-label="userName"></span> <i class="fas fa-sync-alt"></i></button>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-key"></i></div></div>
                                                                <input type="password" name="senha" class="form-control" id="formLoginInputSenha" placeholder="SENHA">
                                                                <div class="input-group-append"><button data-toggle="seePassword" data-target="formLoginInputSenha" class="btn btn-default far fa-eye" type="button"></button></div>
                                                            </div>
                                                        </div>

														<?php if($configs['redefine_password']){ ?>
                                                            <p><small>Esqueceu a senha? <a href="#" data-toggle="redefinirSenha">Redefinir senha</a></small></p>
														<?php } ?>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="text-right">
                                                            <button type="submit" class="btn btn-primary btn-lliure text-uppercase">
                                                                <span class="btn-status-default">login</span>
                                                                <span class="btn-status-disabled">Loading...</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>

                                        <div class="carousel-item">
                                            <form id="formRedefineStart" action="<?php echo ll::$data->url->endereco; ?>forgotPassword" method="post">
                                                <fieldset>
                                                    <div class="modal-body py-0">
                                                        <h2 class="text-center h3 pb-1">Esqueceu a senha</h2>
                                                        <div class="form-group text-center">
                                                            <button data-toggle="trocarUser" type="button" class="btn btn-outline-dark" style="border-radius: 100px;"><span data-label="userName"></span> <i class="fas fa-sync-alt"></i></button>
                                                        </div>

                                                        <p>Foi enviado um e-mail para <strong data-label="userEmail"></strong> com os procedimento para continuar redefinindo sua senha.</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="text-right">
                                                            <button type="submit" class="btn btn-primary btn-lliure text-uppercase">
                                                                <span class="btn-status-default">Tentar novamente</span>
                                                                <span class="btn-status-disabled">Loading...</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>

                                        <div class="carousel-item<?php echo (($resetPassword)? ' active': ''); ?>">
                                            <form id="formRedefineEnd" action="<?php echo ll::$data->url->endereco; ?>" method="post">
                                                <fieldset disabled>
                                                    <div class="modal-body py-0">
                                                        <h2 class="text-center h3 pb-1">Redefinindo a senha</h2>

                                                        <fieldset id="fieldsetRedefinePasswordLoad" class="text-center py-3">
                                                            <i class="fas fa-spinner fa-5x fa-pulse"></i>
                                                        </fieldset>

                                                        <fieldset id="fieldsetRedefinePasswordHidden" hidden disabled>
                                                            <div class="form-group text-center">
                                                                <button data-toggle="trocarUser" type="button" class="btn btn-outline-dark" style="border-radius: 100px;"><span data-label="userName"></span> <i class="fas fa-sync-alt"></i></button>
                                                            </div>

                                                            <input id="formRedefineEndInputId" type="hidden" name="id" value="<?php echo $id; ?>">
                                                            <input id="formRedefineEndInputToken" type="hidden" name="token" value="<?php echo $token; ?>">

                                                            <div class="card bg-light mb-3">
                                                                <div class="card-body">
                                                                    <h3 class="card-title mb-2">Recomendações</h3>
                                                                    <p class="card-text"><small>
                                                                        Recomendamos que escolha uma senha forte com letras maiúsculas e minúsculas
                                                                        números e símbolos e com no mínimo 8 caracteres, não coloque datas e nem palavras
                                                                        conhecidas, e caso queira aumentar mais a força de sua senha utilize no mínimo
                                                                        16 caracteres. Seguindo esses recomendações sua senha sera forte e a chance
                                                                        de alguém mau intencionado descobrir sua senha diminui bastante.
                                                                    </small></p>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-key"></i></div></div>
                                                                    <input type="password" name="senha[]" class="form-control" id="formRedefineEndInputPassword1" placeholder="NOVA SENHA">
                                                                    <div class="input-group-append"><button data-toggle="seePassword" data-target="formRedefineEndInputPassword1" class="btn btn-default far fa-eye" type="button" tabindex="-1"></button></div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-key"></i></div></div>
                                                                    <input type="password" name="senha[]" class="form-control" id="formRedefineEndInputPassword2" placeholder="REPITIR SENHA">
                                                                    <div class="input-group-append"><button data-toggle="seePassword" data-target="formRedefineEndInputPassword2" class="btn btn-default far fa-eye" type="button" tabindex="-1"></button></div>
                                                                </div>
                                                            </div>
                                                        </fieldset>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="text-right">
                                                            <button type="submit" class="btn btn-primary btn-lliure text-uppercase">
                                                                <span class="btn-status-default">Redefinir</span>
                                                                <span class="btn-status-disabled">Loading...</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="container-fluid text-right">
                <a href="<?php echo $personaData['link']; ?>" class="ll_color-700 ll_color-800-hover" target="_blank"><?php echo $personaData['assinatura']; ?></a>
            </div>
        </footer>
    </div>
</section>

<script type="text/javascript">
    (($, D)=>$(()=>{

        const formUser = D.querySelector('#formUser');
        const formPassword = D.querySelector('#formPassword');
        const formRedefineStart = D.querySelector('#formRedefineStart');
        const formRedefineEnd = D.querySelector('#formRedefineEnd');

        const fieldsetUser = D.querySelector('#formUser > fieldset');
        const fieldsetPassword = D.querySelector('#formPassword > fieldset');
        const fieldsetRedefineStart = D.querySelector('#formRedefineStart > fieldset');
        const fieldsetRedefineEnd = D.querySelector('#formRedefineEnd > fieldset');

        const fieldsetRedefinePasswordLoad = D.querySelector('#fieldsetRedefinePasswordLoad');
        const fieldsetRedefinePasswordHidden = D.querySelector('#fieldsetRedefinePasswordHidden');

        const inputUser = D.querySelector('#formLoginInputUsuario');
        const inputSenha = D.querySelector('#formLoginInputSenha');
        const inputId = D.querySelector('#formRedefineEndInputId');
        const inputToken = D.querySelector('#formRedefineEndInputToken');
        const inputSenha1 = D.querySelector('#formRedefineEndInputPassword1');
        const inputSenha2 = D.querySelector('#formRedefineEndInputPassword2');

        const $labelName = $('[data-label="userName"]');
        const $labelEmail = $('[data-label="userEmail"]');
        const btnlabelName = $('[data-toggle="trocarUser"]');
        const btnRedefinirSenha = $('[data-toggle="redefinirSenha"]');
        const btnEye = $('[data-toggle="seePassword"]');
        const $carousel = $('#carouselLoginSteps');
        let dataUser = {};

        function validInit($filed){
            if(!$filed[0].validStart) return;
            $filed.attr('data-valid', 'true');
        }

        function validMinLengthField($filed, min){
            if(!$filed[0].validStart) return;

            min = Math.max(min, 0);
            const value = $filed.val();
            const valid = $filed.attr('data-valid') === 'true';
            const result = value.length >= min;
            $filed.toggleClass('is-invalid', !result);
            $filed.attr('data-valid', ((valid && result)? 'true': 'false'));

            return result;
        }

        function validIsEqualTo($this, $toThis){
            if(!$this[0].validStart) return;
            const valid = $this.attr('data-valid') === 'true';

            const value = $this.val();
            const equalTo = $toThis.val();
            const result = value === equalTo;

            $this.toggleClass('is-invalid', !result);
            $this.attr('data-valid', ((valid && result)? 'true': 'false'));

            return result;
        }

        function valid($filed){
            $filed[0].validStart = true;
            $filed.triggerHandler('input');
            return ($filed.attr('data-valid') === 'true');
        }

        function validReset($filed){
            $filed[0].validStart = false;
            $filed.removeAttr('data-valid');
            $filed.removeClass('is-invalid');
        }

        $(inputUser).on('input', function (){ validInit($(this)); validMinLengthField($(this), 3) });
        $(inputSenha).on('input', function (){ validInit($(this)); validMinLengthField($(this), 8) });

        $(inputSenha1).on('input', function (){ validInit($(this)); validMinLengthField($(this), 8) });
        $(inputSenha2).on('input', function (){ validInit($(this)); validMinLengthField($(this), 8); validIsEqualTo($(this), $(inputSenha1)) });

        function setUser(data){
            dataUser = data;
            fieldsetUser.disabled = false;
            $labelName.html(dataUser.name);
            $labelEmail.html(dataUser.email);
            validReset($(inputSenha).val(''));
        }

        function startUser(){
            inputUser.focus();
            inputUser.select();
        }

        function startPassword(){
            setTimeout(function (){
                inputSenha.focus();
                inputSenha.select();
            }, 1000);
        }

        formUser.addEventListener('submit', function (e){
            e.stopPropagation();
            e.preventDefault();

            if(!valid($(inputUser))){
                (new Vigile).danger('Login inválido');
                return false;
            }

            const method = 'POST';
            const body = new FormData(formUser);
            fieldsetUser.disabled = true;


            fetch( formUser.getAttribute('action'), { method, body }).then(r => r.text()).then(r => {
                const {error, code, message, data} = JSON.parse(r);
                return new Promise(((resolve, reject) => (((typeof error === "undefined") || error === true)? reject({code, message}): resolve(data))));
            }).then(
                function(data){
                    setUser(data);
                    startPassword();
                    $carousel.carousel(1);

                }, function (r) {
                    console.log(r.code, r.message);
                    (new Vigile).danger('Erro no login');
                    fieldsetUser.disabled = false;
                }
            );

            return false;
        });

        $(btnEye).click(function (){
            const $this = $(this);
            const $target = $('#' + $this.attr('data-target'));
            const visible = $target.attr('type') === 'text';

            if(visible){
                $target.attr('type', 'password');
                $this.removeClass('fa-eye-slash').addClass('fa-eye');
                return;
            }

            $target.attr('type', 'text');
            $this.removeClass('fa-eye').addClass('fa-eye-slash');
        })

        btnlabelName.click(function (){
            dataUser = {};
            $labelName.html('');
            $labelEmail.html('');
            validReset($(inputUser).val(''));
            $carousel.carousel(0);
        });

        btnRedefinirSenha.click(function (e){
            e.stopPropagation();
            e.preventDefault();
            $carousel.carousel(2);
            redefineStart();
            return false;
        });

        formPassword.addEventListener('submit', function (e){
            e.stopPropagation();
            e.preventDefault();

            if(!valid($(inputSenha))){
                (new Vigile).danger('Senha inválida');
                return false;
            }

            const method = 'POST';
            const body = new FormData(formPassword);
            body.append('login', dataUser.login);
            fieldsetPassword.disabled = true;

            fetch( formPassword.getAttribute('action'), { method, body }).then(r => r.text()).then(r => {
                const {error, code, message, location} = JSON.parse(r);
                return new Promise(((resolve, reject) => (((typeof error === "undefined") || error === true)? reject({code, message}): resolve(location))));
            }).then(
                function(location){
                    (new Vigile).success('Logado com sucesso');
                    setTimeout(() => window.location = location, 1000);

                }, function (r) {
                    console.log(r.code, r.message);
                    (new Vigile).danger('Senha incorreta');
                    fieldsetPassword.disabled = false;
                }
            );

            return false;
        });

        formRedefineStart.addEventListener('submit', function (e){
            e.stopPropagation();
            e.preventDefault();
            redefineStart();
            return false;
        });

        function redefineStart(){
            const method = 'POST';
            const body = new FormData(formRedefineStart);
            body.append('login', inputUser.value);
            fieldsetRedefineStart.disabled = true;

            fetch( formRedefineStart.getAttribute('action'), { method, body }).then(r => r.text()).then(r => {
                const {error, code, message} = JSON.parse(r);
                return new Promise(((resolve, reject) => (((typeof error === "undefined") || error === true)? reject({code, message}): resolve())));
            }).then(
                function(){
                    (new Vigile).success('E-mail enviado com sucesso');
                    fieldsetRedefineStart.disabled = false;

                }, function (r) {
                    console.log(r.code, r.message);
                    (new Vigile).danger('Erro no processo de redefição de senha');
                    fieldsetRedefineStart.disabled = false;
                }
            );
        }

        function checkTokem(){
            const method = 'POST';
            const body = new FormData();
            body.append('id', $(inputId).val());
            body.append('token', $(inputToken).val());
            fieldsetRedefineEnd.disabled = true;

            fetch( formRedefineEnd.getAttribute('action') + 'checkToken', { method, body }).then(r => r.text()).then(r => {
                const {error, code, message, data} = JSON.parse(r);
                return new Promise(((resolve, reject) => (((typeof error === "undefined") || error === true)? reject({code, message}): resolve(data))));
            }).then(
                function(data){
                    setUser(data);
                    $(fieldsetRedefinePasswordLoad).attr('hidden', true);
                    $(fieldsetRedefinePasswordHidden).removeAttr('hidden').prop('disabled', false);
                    fieldsetRedefineEnd.disabled = false;

                }, function (r) {
                    console.log(r.code, r.message);
                    (new Vigile).danger('Não foi possivel validar o token enviado');
                    setTimeout(() => $carousel.carousel(0), 2000);
                }
            );
        }

        formRedefineEnd.addEventListener('submit', function (e){
            e.stopPropagation();
            e.preventDefault();

            if(!valid($(inputSenha1)) && !valid($(inputSenha2))){
                (new Vigile).danger('Senhas inválidas');
                return false;
            }

            const method = 'POST';
            const body = new FormData(formRedefineEnd);
            fieldsetRedefineEnd.disabled = true;

            fetch( formRedefineEnd.getAttribute('action') + 'resetPassword', { method, body }).then(r => r.text()).then(r => {
                const {error, code, message} = JSON.parse(r);
                return new Promise(((resolve, reject) => (((typeof error === "undefined") || error === true)? reject({code, message}): resolve())));
            }).then(
                function(){
                    (new Vigile).success('Redefinição de senha efetuada com sucesso');
                    setTimeout(() => $carousel.carousel(1), 2000);

                }, function (r) {
                    console.log(r.code, r.message);
                    (new Vigile).danger('Erro ao redefinir a senha');
                    fieldsetRedefineEnd.disabled = false;
                }
            );

            return false;
        });

        if(!!$(inputId).val().length && !!$(inputToken).val().length)
            checkTokem();
        else
            startUser();

        gsqul();
    }))(jQuery, document);
</script>