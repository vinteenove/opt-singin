<?php

namespace Opt\Singin\Tools;

use LliurePanel\ll;
use Senha\Senha;

class User
{

	public static function generateUrlReforgePassord($userId, $hash){
		$token = self::passwordToHash($hash);
		return ll::$data->url->endereco . "{$userId}/{$token}";
	}

	public static function passwordToHash($password){
		return Senha::create($password);	
	}

	public static function validateHash($password, $hash){
		return Senha::valid($password, $hash);
	}

}