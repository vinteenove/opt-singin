<?php

namespace Opt\Singin;

use Api\Vigile\Vigile;
use Etc\Persona\PersonaTheme;
use Helpers\HtmlHelper;
use Helpers\HttpHelper;
use League\Plates\Engine;
use Liberacao\Liberacao;
use Lliure\SimpleCrudRouter\RouteCreatorTrait;
use LliureCore\Controller;
use LliurePanel\Middleware\NeedUserMiddleware;
use LliurePanel\Routable;
use LliurePanel\Strategy\JsonStrategy;
use Opt\Singin\Controllers\Login;
use Opt\Singin\Models\Users;
use Opt\Singin\Controllers\Users as UserController;
use Persona\Persona;
use Router\RouteGroup;
use Router\Router;
use Senha\Senha;
use LliurePanel\ll;
use Token\Token;



class Singin implements
    Controller,
    Routable
{

	private static $configs = [];

    use RouteCreatorTrait{
        RouteCreatorTrait::router as tRouter;
    }

    //protected static string $model = Users::class;
    //protected static string $routeGroup = "";

    public static function router(): Router{
        //$router = self::tRouter();

        $router = new Router;

        //$router->get('/*', [self::class, 'index']);

        if(!ll::autentica()){
            $router->group('/', function(RouteGroup $router){

                $router->get('/', [Login::class, 'index'])->setName('IndexNLI');
                $router->get('/{id}/{token}', [Login::class, 'index']);
                $router->get('/*', [Login::class, 'urlTrap'])->setStrategy(new JsonStrategy);

            });
            $router->group('/', function(RouteGroup $router){

				$router->post('/login', [Login::class, 'login']);
				$router->post('/loginExist', [Login::class, 'loginExist']);
				$router->post('/forgotPassword', [Login::class, 'forgotPassword']);
				$router->post('/checkToken', [Login::class, 'checkToken']);
				$router->post('/resetPassword', [Login::class, 'resetPassword']);

            })->setStrategy(new JsonStrategy);
        }else{
            $router->group('/', function(RouteGroup $route){

                $route->get('/', [UserController::class, 'index']);
                $route->get('/{id}', [UserController::class, 'form']);
                $route->post('/', [UserController::class, 'onserver'])->setName('Singin.user.save');
                $route->get('/minhaconta', [UserController::class, 'minhaConta'])->setName('systemAccount');
                $route->get('/logoff', [UserController::class, 'logoff'])->setName('systemLogoff');

            })->middleware(new NeedUserMiddleware(new SinginLogger('Singin', [Singin::class, 'startSession'])));
        }

        return $router;
    }

    public static function engine(): Engine{
        return new Engine(__DIR__ . '/Views/' . PersonaTheme::getThemerName());
    }

    public static function validate($login, $senha)
    {
        $user = Users::findByLogin($login);

        if (empty($user) || !Senha::valid($senha, $user['hash'])) {
            return false;
        }

        unset($user['senha']);

        return $user;
    }

	public static function configs(): array{

    	if(!empty(self::$configs)){
    		return self::$configs;
		}

		$file = HttpHelper::path(ll::baseDir() . '/opt/Singin/Supplements/singin.ini');
		$configs = parse_ini_file($file, true, INI_SCANNER_TYPED);

    	if(file_exists($file = HttpHelper::path(ll::baseDir() . '/etc/Singin/singin.ini'))){
			$configs = array_replace($configs, parse_ini_file($file, true, INI_SCANNER_TYPED));
		}

		return self::$configs = $configs;
    }


    public static function index(){
        if (!empty($_POST)) {
            self::save();
            return null;
        } else {
            \Jquery\Jquery::build();
            \Api\Navigi\Navigi::build();
            \Api\Vigile\Vigile::build();
            \Api\Jfbox\Jfbox::build();
            \Api\Appbar\Appbar::build();
            \Api\Fileup\Fileup::build();


            if(isset($_GET['en']) && $_GET['en'] == 'minhaconta'){
                $_GET['user'] = ll::$data->user->id;
            }

            \Persona\Persona::add(__DIR__ . '/user.css', 'css');

            if (isset($_GET['user']) && !empty($_GET['user'])) {
                $dados = Users::load($_GET['user'])->toArray();

                return self::engine()->render("singin.form", $dados);
            }else{
                return self::engine()->render("singin.list", []);
            }
        }
    }

    public static function login()
    {
        $error = false;

        if ((!empty($_POST['usuario'])) && (!empty($_POST['senha'])) && Token::valid($_POST['token'])) {
            $error = !(($dados = Singin::validate($_POST['usuario'], $_POST['senha'])) && (ll::autentica($dados['login'], $dados['name'], $dados['group'])));
        }

        if (!$error) {
            $retorna_page = ll::$data->url->endereco;

            if (!empty($_SESSION['ll']['retorno'])) {
                $retorna_page = $_SESSION['ll']['retorno'];
                unset($_SESSION['ll']['retorno']);
            }

            echo json_encode(['error' => false, 'location' => $retorna_page]);
        } else {
            echo json_encode(['error' => true, 'location' => '', 'code' => 1]);
        }
    }

    /**
     *
     */
    public static function logout(){
        ll::desautentica();
        header('location: '. ll::$data->url->endereco);
    }

    /**
     * @return Persona|string|null
     */
    public static function formLogin(){
        if (!empty($_POST)) {
            self::login();

            return null;
        } else {
            return self::engine()->render("homeLogin", []);
        }
    }


    /**
     * @return Persona|string|null
     */
    public static function controller()
    {
        if (ll::autentica()) {
            switch (isset($_GET[0]) && $_GET[0]) {
                case 'logout':
                    self::logout();
                    break;
                default:
                    return self::index();
                    break;
            }
        } else {
            return self::formLogin();
        }
    }

    /**
     * @throws \Exception
     */
    public static function save(){
        switch (isset($_GET['ac']) ? $_GET['ac'] :  ''){
            case 'grava':
                if(!empty($_POST['password'])){
                    $_POST['hash'] = Senha::create($_POST['password']);
                }
                unset($_POST['password']);

                $file = new \Api\Fileup\Fileup();
                $file->diretorio = '../uploads/usuarios/';
                $file->up();

                if(ll::valida() || $_SESSION['ll']['user']['id'] == $_POST['id']) {
                    if(isset($_POST['liberacoes'])) {
                        $liberacoes = $_POST['liberacoes'];
                        unset($_POST['liberacoes']);
                    }

                    Users::build($_POST)->update();

                    if(ll::valida() && isset($liberacoes)){
                        $liberacao = new Liberacao();
                        $delete = $insert = array();

                        foreach( $liberacao->get(array('login' => $_POST['login'])) as $del) {
                            $delete["{$del['operation_type']}/{$del['operation_key']}"] = $del;
                        }

                        foreach ($liberacoes as $v){
                            if(isset($delete[$v]))
                                unset($delete[$v]);
                            else{
                                list($operation_type, $operation_load) = explode('/', $v);
                                $insert[] = array(
                                    'operation_type' => $operation_type,
                                    'operation_key' => $operation_load,
                                    'login' => $_POST['login'],
                                );
                            }
                        }

                        if (!empty($delete)) {
                            foreach ($delete as $del) {
                                $liberacao->del($del);
                            }
                        }

                        if (!empty($insert)) {
                            foreach ($insert as $ins) {
                                $liberacao->set($ins);
                            }
                        }

                    }
                }

                Vigile ::success("Alteração realizada com sucesso!");
                header('Location: '. ll::$data->url->endereco . ll::$data->app->home . (isset($_GET['en']) && $_GET['en'] == 'minhaconta' ? 'en=minhaconta' : '' ));

                break;
            case 'new':
                Users::build(['name' => 'Novo usuario'])->insert();
                break;
        }
    }



    /**
     * @param string $entryPath
     * @return null
     */
//    public static function needPermission(array $permission){
//        return new class($permission) implements MiddlewareInterface{
//
//            private array $permission;
//
//            public function __construct($permission){
//                $this->permission = $permission;
//            }
//
//            public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface{
//
//                /*if(!Singin::userPermission($permission)){
//                    //return retorna a tela de bloqueio
//                }*/
//
//                return $handler->handle($request);
//            }
//
//            public function __call($name, $arguments){
//                $this->permission[$name] = true;
//                return $this;
//            }
//
//            public function desktopInsert($sdfsdfsdf){
//
//
//            }
//        };
//    }



    public static function createUser($question, $handle): bool{

        /*$question->rule;
        $question->user;


        if()


        $status = $handle->dispath($user);

        return (($user['group'] == 'admin')? true: $status);*/
    }


    public static function startSession($app){




        return [];
    }


}