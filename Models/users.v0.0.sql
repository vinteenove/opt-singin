ALTER TABLE `plugin_lliure_admin`
CHANGE COLUMN `senha` `hash` VARCHAR(200) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `login`,
CHANGE COLUMN `nome` `name` VARCHAR(200) NOT NULL COLLATE 'latin1_swedish_ci' AFTER `hash`,
CHANGE COLUMN `foto` `photo` VARCHAR(256) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `email`,
CHANGE COLUMN `grupo` `group` VARCHAR(10) NOT NULL DEFAULT 'user' COLLATE 'latin1_swedish_ci' AFTER `photo`,
DROP COLUMN `twitter`;