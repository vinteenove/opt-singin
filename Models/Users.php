<?php

namespace Opt\Singin\Models;

use dibi;
use LliureCore\Model;
use Senha\Senha;

class Users extends Model
{
	protected static $primaryKey = 'id';
    protected static ?string $table = 'singin_users';

    public static function findByLogin($login){
        $table = static::getTable();
        return static::findOne('select * from ' . $table . ' where `login` = ?', $login);
    }

    public static function findById($id){
        return static::load($id);
    }

    public static function exist(string $login, string $senha){
        $user = self::findByLogin($login);

        if(empty($user)){
        	return null;
		}

        if(md5($senha) === $user['hash']){
			$user['hash'] = Senha::create($senha);
			$user->update();
		}

        if(!Senha::valid($senha, $user['hash'])){
        	return null;
		}

        unset($user['hash']);
        return $user;
    }

}