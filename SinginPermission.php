<?php


namespace Opt\Singin;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

trait SinginPermission{

//    public static function needPermission(array $permission): MiddlewareInterface{
//        return new class($permission) implements MiddlewareInterface{
//
//            private array $permission;
//
//            public function __construct($permission){
//                $this->permission = $permission;
//            }
//
//            public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface{
//
//                /*if(!Singin::userPermission($permission)){
//                    //return retorna a tela de bloqueio
//                }*/
//
//                return $handler->handle($request);
//            }
//
//            public function __call($name, $arguments){
//                $this->permission[$name] = true;
//                return $this;
//            }
//        };
//    }
//
//    private array $ruleMap = [];
//
//    public function __construct($appKey){
//        $userPermission = $this->loadPermission($appKey);
//    }
//
//    protected function map(string $roule, string $feature, $handler): Rule{
//
//    }
//
//    public function any(string $feature, $handler): Rule{
//        return $this->map('*', $feature, $handler);
//    }
//
//    public function admin(string $feature, $handler): Rule{
//        return $this->map('admin', $feature, $handler);
//    }
//
//    public function user(string $feature, $handler): Rule{
//        return $this->map('user', $feature, $handler);
//    }
//
//
//    public function hasPermission($feature){
//
//    }

    /**
     * @return string
     */
    abstract protected static function getAppKey(): string;

	/**
	 * @return bool
	 */
	public static function isDev(): bool{
		return SinginLogger::getUser()->group === 'dev';
	}

    /**
     * @return bool
     */
    public static function isAdmin(): bool{
        return self::isDev() || SinginLogger::getUser()->group === 'admin';
    }

    /**
     * @return bool
     */
    public static function isUser(): bool{
        return self::isDev() || SinginLogger::getUser()->group === 'user';
    }

    /**
     * @param string $feature
     * @return bool
     */
    public static function hasPermission(string $feature): bool{
        return self::isDev() || in_array($feature, SinginLogger::getAppUserFeature(static::getAppKey()));
    }

}